package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	
	public void init() throws ServletException{
		System.out.println("*******************************************");
		System.out.println(" UserServlet has been initialized");
		System.out.println("*******************************************");
	}
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String contactNumber = req.getParameter("contactNumber");
        
        // System Properties
        System.setProperty("firstName", firstName);
        
        //HttpSession
        HttpSession session = req.getSession();
        session.setAttribute("lastName", lastName);
        
        // ServletContext
        ServletContext srvContext = getServletContext();
        srvContext.setAttribute("email", email);
        
        // URL rewriting via sendRedirect method
        res.sendRedirect("details?contactNumber="+contactNumber);
			
	}
	
	
	public void destroy() {
		System.out.println("*******************************************");
		System.out.println(" UserServlet has been destroyed.");
		System.out.println("*******************************************");
	}

}
